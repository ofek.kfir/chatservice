import socket
import sys
from threading import Thread


    
class Server:
    def __init__(self, ip, port):


        """
        a server class

        ip : str 
            The target ip of the server 
        
        port : int
            The target port of the server
        """
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.ip = str(ip)
        self.port = int(port)
        self.clientList = {}
        
        
        

    

    def __broadcast(msg, name):
        for sock in clientList:
            sock.send(bytes("{}: {}".format(name, msg),'utf8'))


    def clientHandler(self,conn):
        name = conn.recv(2048).decode("utf8")
        conn.send(bytes("welcome {}".format(),"utf8"))
        self.clientList[conn] = name

        while True:
            try:
                msg = conn.recv(2048).decode("utf8")
                __broadcast(msg, name)
            except KeyboardInterrupt:
                conn.close()
                self.clientList.remove(conn)
                break
    def accept_connections(self):
        while True:
            client, client_address= self.server.accept()
            client.send(bytes("enter name:",'utf8'))
            Thread(target=self.clientHandler, args=(client,)).start()
    def run(self):
        self.server.bind((self.ip, self.port))
        self.server.listen(100)
        THREAD = Thread(target=self.accept_connections).start()
        self.server.close()

